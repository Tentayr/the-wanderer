import pygame
from pygame.locals import *

#Définition de notre class
class Personnage:

    # Notre méthode constructeur
    def __init__(self):
        self.image = pygame.image.load("persoimage/up/link3up.png").convert_alpha()
        self.position_perso = self.image.get_rect(x=600,y=450)
        self.state = 0
        self.position_suivante = []

    # Methode pour afficher le personnage
    def afficherPersonnage(self,ecran):
        ecran.blit(self.image, self.position_perso)

    # Methode pour deplacer le personnage en bas
    def deplacerPersonnageBas(self):

        if self.state == 0:
            self.image = pygame.image.load("persoimage/down/link1down.png").convert_alpha()
            self.state = 1

        elif self.state == 1:
            self.image = pygame.image.load("persoimage/down/link3down.png").convert_alpha()
            self.state = 2

        elif self.state == 2:
            self.image = pygame.image.load("persoimage/down/link2down.png").convert_alpha()
            self.state = 3

        elif self.state == 3:
            self.image = pygame.image.load("persoimage/down/link3down.png").convert_alpha()
            self.state = 0

        self.position_perso = self.position_perso.move(0,3)

    # Methode pour deplacer le personnage en haut
    def deplacerPersonnageHaut(self):

        if self.state == 0:
            self.image = pygame.image.load("persoimage/up/link1up.png").convert_alpha()
            self.state = 1

        elif self.state == 1:
            self.image = pygame.image.load("persoimage/up/link3up.png").convert_alpha()
            self.state = 2

        elif self.state == 2:
            self.image = pygame.image.load("persoimage/up/link2up.png").convert_alpha()
            self.state = 3

        elif self.state == 3:
            self.image = pygame.image.load("persoimage/up/link3up.png").convert_alpha()
            self.state = 0

        self.position_perso = self.position_perso.move(0,-3)

    # Methode pour deplacer le personnage a gauche
    def deplacerPersonnageGauche(self):

        if self.state == 0:
            self.image = pygame.image.load("persoimage/left/link1left.png").convert_alpha()
            self.state = 1

        elif self.state == 1:
            self.image = pygame.image.load("persoimage/left/link3left.png").convert_alpha()
            self.state = 2

        elif self.state == 2:
            self.image = pygame.image.load("persoimage/left/link2left.png").convert_alpha()
            self.state = 3

        elif self.state == 3:
            self.image = pygame.image.load("persoimage/left/link3left.png").convert_alpha()
            self.state = 0

        self.position_perso = self.position_perso.move(-3,0)

    # Methode pour deplacer le personnage a droite.
    def deplacerPersonnageDroite(self):

        if self.state == 0:
            self.image = pygame.image.load("persoimage/right/link1right.png").convert_alpha()
            self.state = 1

        elif self.state == 1:
            self.image = pygame.image.load("persoimage/right/link3right.png").convert_alpha()
            self.state = 2

        elif self.state == 2:
            self.image = pygame.image.load("persoimage/right/link2right.png").convert_alpha()
            self.state = 3

        elif self.state == 3:
            self.image = pygame.image.load("persoimage/right/link3right.png").convert_alpha()
            self.state = 0


        self.position_perso = self.position_perso.move(3,0)

    def retournerPositionSuivanteBas(self):
        self.position_suivante = self.position_perso.move(0,3)  #Bas

    def retournerPositionSuivanteHaut(self):
        self.position_suivante = self.position_perso.move(0,-3) #Haut

    def retournerPositionSuivanteGauche(self):
        self.position_suivante = self.position_perso.move(-3,0) #Gauche

    def retournerPositionSuivanteDroite(self):
        self.position_suivante = self.position_perso.move(3,0)  #Droite



    def transitionPersonnageBas(self):
        self.position_perso = self.position_perso.move(0,-570)  #Bas

    def transitionPersonnageHaut(self):
        self.position_perso = self.position_perso.move(0,570)  #Bas

    def transitionPersonnageDroite(self):
        self.position_perso = self.position_perso.move(0,1170)  #Bas

    def transitionPersonnageGauche(self):
        self.position_perso = self.position_perso.move(0,-1170)  #Bas
