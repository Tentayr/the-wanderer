#Importation des librairies
import pygame
import random
import sys
from pygame.locals import *
from ClassMapReading import *
from ClassPersonnage import *
from ClassPNJ import *
from ClassCollision import *
from ClassAssetsReading import *

#Initialisation de pygame
pygame.init()

#Ouverture de l'ecran de jeu
ecran = pygame.display.set_mode((1200, 600))

#Insertion de la bande son
son = pygame.mixer.Sound("music/darkworld.wav")
bump = pygame.mixer.Sound("sound/bump.wav")

#demarrage de la bande son
son.play(loops=-1)

#Creation du hero
hero = Personnage()

#Creation variable carte
carteNum = 1

#Temps de maintien pour refaire une action
pygame.key.set_repeat(400, 150)

#boucle infinie
lancement = 1

while lancement:
    pygame.time.Clock().tick(500)         #Brider le processeur, repetition evenement toutes les 400ms
    for event in pygame.event.get():      #Attente des événements

        if event.type == QUIT: #si un evenement de type cliquer sur la croix ou echap
            lancement = 0      #on arrête la boucle et donc on sort du jeu.

        if( pygame.key.get_pressed()[pygame.K_DOWN] != 0):   #si on presse la touche fleche bas
            hero.retournerPositionSuivanteBas()
            if hero.position_suivante.collidelist(collide.tableaucolision) == -1:
                if hero.position_suivante.collidelist(collide.tableautransitionbas) == -1:
                    hero.deplacerPersonnageBas()
                else:
                    carteNum -= 1
                    hero.transitionPersonnageBas()
            else:
                bump.play()

        if( pygame.key.get_pressed()[pygame.K_UP] != 0):
            hero.retournerPositionSuivanteHaut()
            if hero.position_suivante.collidelist(collide.tableaucolision) == -1:
                if hero.position_suivante.collidelist(collide.tableautransitionhaut) == -1:
                    hero.deplacerPersonnageHaut()
                else:
                    carteNum += 1
                    hero.transitionPersonnageHaut()
            else:
                bump.play()

        if( pygame.key.get_pressed()[pygame.K_LEFT] != 0):   #si on presse la touche fleche gauche
            hero.retournerPositionSuivanteGauche()
            if hero.position_suivante.collidelist(collide.tableaucolision) == -1:
                if hero.position_suivante.collidelist(collide.tableautransitiongauche) == -1:
                    hero.deplacerPersonnageGauche()
                else:
                    carteNum += 100
                    hero.transitionPersonnageGauche()
            else:
                bump.play()

        if( pygame.key.get_pressed()[pygame.K_RIGHT] != 0):  #si on presse la touche fleche droite
            hero.retournerPositionSuivanteDroite()
            if hero.position_suivante.collidelist(collide.tableaucolision) == -1:
                if hero.position_suivante.collidelist(collide.tableautransitiondroite) == -1:
                    hero.deplacerPersonnageDroite()
                else:
                    carteNum -= 100
                    hero.transitionPersonnageDroite()

            else:
                bump.play()
        if( pygame.key.get_pressed()[pygame.K_a] != 0): #si on presse la touche a
            lancement = 0 #on arrete la boucle
            son.stop()
            exec(open("./menu.py").read()) #on lance le menu
            sys.exit() #on sort du programme en court



        #Ouverture de la map
        carte = MapReading(str(carteNum)+".png")

        #Generation des assets
        assets = AssetsReading(str(carteNum)+"-assets.png")

        #Generation des colisions
        collide = MapCollision(str(carteNum))
        collide.lireCollision()
        collide.CreerColision(5,5)

        #zone de test


        #actualisation
        carte.afficherMap(ecran)                 #on reaffiche la carte
        hero.afficherPersonnage(ecran)           #on replace le héro (pour actualiser sa position)
        assets.afficherAssets(ecran)             #on actualiser les assets
        pygame.display.flip()                    #on actualise l'écran
