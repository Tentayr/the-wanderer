import pygame
from pygame.locals import *

class MapCollision:

    # Notre méthode constructeur
    def __init__(self,fichier): #on demande le nom de la map de colision a charger.

        self.nomfichier = fichier+".txt" #On affecte le nom du fichier et on lui ajoute l'extention txt.
        self.chainecartecolision = "" #On Initialise une chaine qui contiendra les données de la carte.
        self.tableaucolision = [] #On Initialise un tableau qui contiendra les positions interdite au deplacement
        self.tableautransitionbas = [] #On Initialise un tableau qui contiendra les positions de transitions
        self.tableautransitionhaut = [] #On Initialise un tableau qui contiendra les positions de transitions
        self.tableautransitiondroite = [] #On Initialise un tableau qui contiendra les positions de transitions
        self.tableautransitiongauche = [] #On Initialise un tableau qui contiendra les positions de transitions

    # Notre méthode qui ouvre le fichier contenant la map,liste son contenu dans une chaine,ferme la map, et renvoie la chaine
    def lireCollision(self):

        CarteTemp = open("colision/"+self.nomfichier,"r") #On ouvre la map en lecture
        self.chainecartecolision = CarteTemp.read()  #On envoie toute les données de la map dans chainecartecolision.
        CarteTemp.close() #On ferme la map

    # Notre méthode qui affiche la carte sur l'ecran choisi
    def CreerColision(self,intervalleX,intervalleY):
        #Initialisation du curseur mapping
        x = 0
        y = 0
        Curseur = (x,y)

        #Creation des colisions
        for i in self.chainecartecolision: #pour chaque caractère dans chainecartecolision
            if i == '0':                   #si c'est un 0
                x += intervalleX             #on passe a la tile suivante
                Curseur = (x,y)             #on actualise le curseur
            if i == '1':
                self.tableaucolision += [pygame.Rect(x,y,intervalleX,intervalleY)]
                x += intervalleX
                Curseur = (x,y)
            if i == '2':
                self.tableaucolision += [pygame.Rect(x,y,intervalleX,intervalleY)]
                x += intervalleX
                Curseur = (x,y)
            if i == 'H':
                self.tableautransitionhaut += [pygame.Rect(x,y,intervalleX,intervalleY)]
                x += intervalleX
                Curseur = (x,y)
            if i == 'B':
                self.tableautransitionbas += [pygame.Rect(x,y,intervalleX,intervalleY)]
                x += intervalleX
                Curseur = (x,y)
            if i == 'G':
                self.tableautransitiongauche += [pygame.Rect(x,y,intervalleX,intervalleY)]
                x += intervalleX
                Curseur = (x,y)
            if i == 'D':
                self.tableautransitiondroite += [pygame.Rect(x,y,intervalleX,intervalleY)]
                x += intervalleX
                Curseur = (x,y)
            if i == '\n':                  #si c'est un saut de ligne
                y += intervalleY             #on passe a la ligne de tile suivante
                x = 0                       #on passe à la premiere tile de la ligne
                Curseur = (x,y)             #on actualise le curseur

     
