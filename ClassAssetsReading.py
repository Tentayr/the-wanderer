import pygame
from pygame.locals import *

#Définition de notre class
class AssetsReading:

    # Notre méthode constructeur
    def __init__(self,fichier): #on demande le nom de la map a charger.

        self.nomcarteassets = pygame.image.load("assets/"+fichier).convert_alpha()

    # Notre méthode qui affiche la carte sur l'ecran choisi
    def afficherAssets(self,ecran):
        ecran.blit(self.nomcarteassets, (0,0))
