import pygame
from pygame.locals import *

#Définition de notre class
class MapReading:

    # Notre méthode constructeur
    def __init__(self,fichier): #on demande le nom de la map a charger.

        self.nomcarte = pygame.image.load("map/"+fichier).convert()

    # Notre méthode qui affiche la carte sur l'ecran choisi
    def afficherMap(self,ecran):
        ecran.blit(self.nomcarte, (0,0))
