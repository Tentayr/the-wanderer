import pygame
from pygame.locals import *
import random
import time

#Définition de notre class
class PersonnageNonJoueur:

    # Notre méthode constructeur
    def __init__(self,image,x,y):
        self.image = pygame.image.load(image).convert_alpha()     #l'image du PNJ avec l'image rentrer en paramètre.
        self.position_PNJ = (x,y)  #la position du pnj avec les valeurs x et y rentrer en parametre.
        self.state = 0      #on initialise le numéro du mouvement du PNJ
        self.timemove = 0 #on initialise un compteur pour le deplacement aléatoire
    # Méthode pour afficher le PNJ
    def afficherPersonnageNonJoueur(self,ecran):
        ecran.blit(self.image, self.position_PNJ)  #On place le PNJ sur l'écran placer en parametre: # avec la position définie en attribut.
                                                                                                     # avec son image

    def mouvementePersonnageNonJoueur(self):
        if self.timemove == 600:
            self.timemove = 0
            if self.state == 0:
                self.image = pygame.image.load("monsterimage/dragon/static/dragonbas.png").convert_alpha()
                self.state = random.randint(0,3)
            elif self.state == 1:
                self.image = pygame.image.load("monsterimage/dragon/static/dragondroite.png").convert_alpha()
                self.state = random.randint(0,3)
            elif self.state == 2:
                self.image = pygame.image.load("monsterimage/dragon/static/dragongauche.png").convert_alpha()
                self.state = random.randint(0,3)
            elif self.state == 3:
                self.image = pygame.image.load("monsterimage/dragon/static/dragonhaut.png").convert_alpha()
                self.state = random.randint(0,3)
        self.timemove += 1

    def mouvementMenu(self):
            if self.state == 0:
                self.image = pygame.image.load("persoimage/down/link1down.png").convert_alpha()
                time.sleep(0.5)
                self.state = 1

            elif self.state == 1:
                self.image = pygame.image.load("persoimage/down/link3down.png").convert_alpha()
                time.sleep(0.5)
                self.state = 2

            elif self.state == 2:
                self.image = pygame.image.load("persoimage/down/link2down.png").convert_alpha()
                time.sleep(0.5)
                self.state = 3

            elif self.state == 3:
                self.image = pygame.image.load("persoimage/down/link3down.png").convert_alpha()
                time.sleep(0.5)
                self.state = 0

    def redefinirPositionBas(self):
        if self.position_PNJ == (305,165):
            self.position_PNJ = (305,265)

        elif self.position_PNJ == (305,265):
            self.position_PNJ = (305,365)

        elif self.position_PNJ == (305,365):
            self.position_PNJ = (305,165)

    def redefinirPositionHaut(self):
        if self.position_PNJ == (305,165):
            self.position_PNJ = (305,365)

        elif self.position_PNJ == (305,265):
            self.position_PNJ = (305,165)

        elif self.position_PNJ ==  (305,365):
            self.position_PNJ = (305,265)
