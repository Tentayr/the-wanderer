#Importation des librairies
import pygame
import random
import time
from pygame.locals import *
from ClassMapReading import *
from ClassPNJ import *
import sys

#Initialisation de pygame
pygame.init()

#Ouverture de l'ecran de jeu
ecran = pygame.display.set_mode((1200, 600))

#Insertion de la bande son
son = pygame.mixer.Sound("music/lightworld.wav")
select = pygame.mixer.Sound("sound/selection.wav")
game = pygame.mixer.Sound("sound/game.wav")
#demarrage de la bande son
son.play(loops=-1)

#Ouverture de la map
menu = pygame.image.load("map/menu.png").convert()

#Affichage de la carte
ecran.blit(menu,(0,0))

#Affichage des entetes
Jouer = PersonnageNonJoueur("monsterimage/texte/jouer_red.png",185,150)
Jouer.afficherPersonnageNonJoueur(ecran)
Quitter = PersonnageNonJoueur("monsterimage/texte/quitter.png",150,250)
Quitter.afficherPersonnageNonJoueur(ecran)
Credits = PersonnageNonJoueur("monsterimage/texte/credits.png",150,350)
Credits.afficherPersonnageNonJoueur(ecran)
Hero = PersonnageNonJoueur("persoimage/down/link3down.png",305,165)
Hero.afficherPersonnageNonJoueur(ecran)

#Raffraichisssement de l'ecran
pygame.display.flip()

continuer = 1
while continuer:
    Hero.mouvementMenu()
    for event in pygame.event.get():      #Attente des événements
        if event.type == QUIT: #si un evenement de type cliquer sur la croix ou echap
            continuer = 0      #on arrête la boucle et donc on sort du jeu.

        if( pygame.key.get_pressed()[pygame.K_DOWN] != 0):   #si on presse la touche fleche bas
            Hero.redefinirPositionBas()
            if Hero.position_PNJ == (305,165):
                Jouer.image = pygame.image.load("monsterimage/texte/jouer_red.png").convert_alpha()
                Credits.image = pygame.image.load("monsterimage/texte/credits.png").convert_alpha()
                Quitter.image = pygame.image.load("monsterimage/texte/quitter.png").convert_alpha()
            elif Hero.position_PNJ == (305,265):
                Jouer.image = pygame.image.load("monsterimage/texte/jouer.png").convert_alpha()
                Credits.image = pygame.image.load("monsterimage/texte/credits.png").convert_alpha()
                Quitter.image = pygame.image.load("monsterimage/texte/quitter_red.png").convert_alpha()

            elif Hero.position_PNJ == (305,365):
                Jouer.image = pygame.image.load("monsterimage/texte/jouer.png").convert_alpha()
                Credits.image = pygame.image.load("monsterimage/texte/credits_red.png").convert_alpha()
                Quitter.image = pygame.image.load("monsterimage/texte/quitter.png").convert_alpha()
            select.play()

        if( pygame.key.get_pressed()[pygame.K_UP] != 0):   #si on presse la touche fleche bas
            Hero.redefinirPositionHaut()
            if Hero.position_PNJ == (305,165):
                Jouer.image = pygame.image.load("monsterimage/texte/jouer_red.png").convert_alpha()
                Credits.image = pygame.image.load("monsterimage/texte/credits.png").convert_alpha()
                Quitter.image = pygame.image.load("monsterimage/texte/quitter.png").convert_alpha()
            elif Hero.position_PNJ == (305,265):
                Jouer.image = pygame.image.load("monsterimage/texte/jouer.png").convert_alpha()
                Credits.image = pygame.image.load("monsterimage/texte/credits.png").convert_alpha()
                Quitter.image = pygame.image.load("monsterimage/texte/quitter_red.png").convert_alpha()
            elif Hero.position_PNJ == (305,365):
                Jouer.image = pygame.image.load("monsterimage/texte/jouer.png").convert_alpha()
                Credits.image = pygame.image.load("monsterimage/texte/credits_red.png").convert_alpha()
                Quitter.image = pygame.image.load("monsterimage/texte/quitter.png").convert_alpha()
            select.play()

        if( pygame.key.get_pressed()[pygame.K_RETURN] != 0):   #si on presse la touche fleche bas
            if Hero.position_PNJ == (305,165):
                continuer = 0 #on arrete la boucle
                son.stop()
                game.play()
                exec(open("./main.py").read()) #on lance le jeux
                sys.exit() #on sort du programme en court
            elif Hero.position_PNJ == (305,265):
                sys.exit() #on sort du programme en court
            elif Hero.position_PNJ == (305,365):
                sys.exit() #on sort du programme en court (temporaire)



    ecran.blit(menu,(0,0))           #on refait la carte
    Hero.afficherPersonnageNonJoueur(ecran)  #on replace le héro (pour actualiser sa position)
    Credits.afficherPersonnageNonJoueur(ecran)
    Quitter.afficherPersonnageNonJoueur(ecran)
    Jouer.afficherPersonnageNonJoueur(ecran)
    pygame.display.flip()                    #on actualise l'écran
